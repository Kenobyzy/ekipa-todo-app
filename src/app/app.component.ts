import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit { // implements OnInit
  title = 'Todo App';

  itemValue = '';
  items: Array<any[]>;
  selected = [];
  count = 0;
  obv = [];

  // tslint:disable-next-line:space-before-function-paren
  constructor (public db: AngularFireDatabase) {

  }

  add() {
    if (this.itemValue === '') {
      alert('Enter a Task');
    } else {
      this.db.list('/todoItems').push({ content: this.itemValue, done: false });
      this.itemValue = '';
    }
  }

  delete() {
    console.log(this.selected);
    if (this.selected.length === 0) {
      alert('select a Task');
    } else {
      this.selected.forEach(element => {
        this.db.object('/todoItems/' + element.key).remove();
      });
    }
  }

  select(event, item) {
    if (event.checked) {
      item.done = true;
      this.selected.push(item);
      this.db.object('/todoItems/' + item.key)
        .update({ content: item.content, done: item.done });
    } else {
      item.done = false;
      this.selected.pop();
    }
  }
  ngOnInit() {
    this.call().then(objects => {
      this.items = objects;
      this.count = this.items.length;
    }).catch(error => {
      alert('Nothing found');
    });
  }

  call() {
    const ref = this.db.database.ref('/todoItems');
    return new Promise((resolve, reject) => {
      // tslint:disable-next-line:only-arrow-functions
      ref.on('value', function (snapshot) {
        if (snapshot.val() == null) {
          reject(null);
        } else {
          const list = new Array();
          // tslint:disable-next-line:only-arrow-functions
          snapshot.forEach(function (data) {
            const item = {
              key: data.key, // this is to get the ID, if needed
              content: data.val().content,
              done: data.val().done,
            };
            list.push(item);
          });
          resolve(list);
        }
      });
    });
  }
}

